import java.util.Scanner;

class BinarySearch
{
  public static void main(String args[])
  {
    int i, first, last, middle, n, key, array[];

    Scanner in = new Scanner(System.in);
    System.out.println("Enter the number of elements of array");
    n = in.nextInt();
    array = new int[n];

    System.out.println("Enter " + n + " integers");


    for (i = 0; i < n; i++)
      array[i] = in.nextInt();

    System.out.println("Enter the key elements");
    key = in.nextInt();

    first  = 0;
    last   = n - 1;
    middle = (first + last)/2;

    while( first <= last )
    {
      if ( array[middle] < key )
        first = middle + 1;
      else if ( array[middle] == key )
      {
        System.out.println(key + " is present at position " + (middle + 1) + ".");
        break;
      }
      else
         last = middle - 1;

      middle = (first + last)/2;
   }
   if ( first > last )
      System.out.println(key + " is not present in the array.\n");
  }
}